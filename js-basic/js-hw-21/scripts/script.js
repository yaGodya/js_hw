const btn = document.querySelector('.button').addEventListener('click', () => {
    let parentDiv = document.createElement('div');
    document.body.prepend(parentDiv);
    let createCircle = +prompt('Какого диаметра хотите круг?');

    for(i = 0; i < 100; i++) { 
        const randomColorCircle = '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase();
        let divCircle = document.createElement('div');
        divCircle.classList.add('circle');
        divCircle.style.cssText = ` 
                border-radius: 50%;
                display: inline-block;
                height: ${createCircle}px;
                width: ${createCircle}px;
                background-color: ${randomColorCircle};
                margin: 10px;`
                ;
        parentDiv.appendChild(divCircle);

        divCircle.addEventListener('click', () => { 
            divCircle.style.display = 'none';
            console.log(divCircle);
        });
    };
});



