const sliderLine = document.querySelector('.slider-line');
const prevButton = document.querySelector('.prev');
const nextButton = document.querySelector('.next');
const images = document.querySelectorAll('img');
let position = 0;

const nextSlide = () => {
    position < (images.length - 1) * 450 ? position += 450 : position = 0;
    sliderLine.style.left = -position + 'px';
};

nextButton.addEventListener('click', nextSlide);

const prevSlide = () => {
    position > 0 ? position -= 450 : position = (images.length - 1) * 450;
    sliderLine.style.left = -position + 'px';
}
prevButton.addEventListener('click', prevSlide);