const startGameBtn = document.querySelector('.btn-start-game');


startGameBtn.addEventListener('click', () => { 
    startGameBtn.style.display = 'none';

    const parentDiv = document.createElement('div');
    parentDiv.classList.add('parent-div');
    document.body.prepend(parentDiv);

    const minsWrapper = document.createElement('div');
    minsWrapper.classList.add('mins-wrapper');
    parentDiv.prepend(minsWrapper);

    let minsBlock;
    for ( i = 0; i < (8 * 8); i++){
        minsBlock = document.createElement('div');
        minsBlock.classList.add('mins-block');
        minsWrapper.prepend(minsBlock);
    }

})