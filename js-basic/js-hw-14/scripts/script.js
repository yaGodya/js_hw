const theme = document.querySelector('.theme-toggle');
const html = document.querySelector('html');
const span = document.querySelector('.material-symbols-outlined');

theme.addEventListener('click', (event) => {
    event.preventDefault();
    localStorage.getItem('theme') === 'dark' ? localStorage.removeItem('theme') : localStorage.setItem('theme','dark')
    addTheme();
});

function addTheme() { 
    localStorage.getItem('theme') === 'dark' ? 
    (html.classList.add('dark'), span.textContent = 'dark_mode') : 
    (html.classList.remove('dark'), span.textContent = 'wb_sunny')
}

addTheme();