const stopSlide = document.querySelector('.stop');
const resumetSlide = document.querySelector('.resume');
let indexValue = 0;
let isPaused = false;

function slideShow() { 
    setTimeout(slideShow, 3000);
    const img = document.querySelectorAll('img');
    for (let i = 0; i < img.length;i++ ) { img[i].style.display = 'none';};
    if(!isPaused) {indexValue++};
    if(indexValue > img.length) { indexValue = 1;};
    img[indexValue - 1].style.display = 'block';
    stopSlide.addEventListener('click', () => isPaused = true);
    resumetSlide.addEventListener('click', () => isPaused = false);
};
slideShow();