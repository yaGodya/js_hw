function createNewUser() { 
    let firstName = prompt('Укажите ваше имя', '');
    let lastName = prompt('Укажите вашу фамсилию', '');
    let birthday = prompt('Укажите дату, когда вы родились dd.mm.yyyy');

    const newUser={
        firstName,
        lastName,
        getLogin(){
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },

        getAge(){ 
            const date = new Date();
            dd = Number(birthday.slice(0, 2));
            mm = Number(birthday.slice(3, 5));
            yyyy = Number(birthday.slice(6));
            let isBirthday = new Date(yyyy, mm, dd);

            return Math.floor(((Date.parse(date) - Date.parse(isBirthday)) / 60 / 60 /24 / 365 / 1000));
        },

        getPassword(){
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + yyyy;
        },
    };

    return newUser;
}

const user = createNewUser();
console.log('Ваш возраст ' + user.getAge());
console.log('Ваш логин ' + user.getLogin());
console.log('Ваш пароль ' + user.getPassword());
console.log(user);