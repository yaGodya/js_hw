const btns = document.querySelectorAll('.btn');

function paintKey() { 
    document.addEventListener('keydown', (event) => { 
        const elem = document.querySelector(`[data-attr="${event.code}"]`);
        if(!elem) return
        const btnActive = document.querySelector('.btn.active');
        if(btnActive){ 
            btnActive.classList.remove('active');
        }
        elem.classList.toggle('active');
    });
};
paintKey();

