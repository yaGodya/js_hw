// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript:
// Прототипное наследование работает по основам ООП, создавая базовый шаблон мы можем его 
// переиспользовать и масштабировать сохраняя читабельность и краткую стьруктуру кода
/////////////////////////////////////////////////////////////////////////////////////////
// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Данную конструкцию используют для доступа к родительский унаследоваемым свойствам
/////////////////////////////////////////////////////////////////////////////////////////
class Eployee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() { 
        return this._name
    }
    set name(value) {
        this.name = value;
    }

    get age() { 
        return this._age
    }
    set age(value) {
        this._age = value;
    }

    get salary() { 
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Eployee{
    constructor(name, age, salary, lang) {
                super(name, age, salary, lang);
                this.lang = lang
            }

            get lang() {
                return this._lang;
            }
            set lang(value) {
                this._lang = value;
            }

            get salary()  {
                return this._salary * 3;
            }
}

const programmer = new Programmer('George', 28, 30000, 'UKR')
const nextProgrammer = new Programmer('Valeriy', 25, 31000, 'UKR')
console.log(nextProgrammer)
console.log(nextProgrammer.salary);
console.log(programmer);
console.log(programmer.salary);