//////////////////////////////////////////////////////////////////////////////
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

const books = [
    { 
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70 
    }, 
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    }, 
    { 
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    }, 
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.querySelector('#root');

function addNodes() { 
    const ul = document.createElement('ul');
    div.appendChild(ul);
    for ( let key of books ) { 
        try {
            if(!key.hasOwnProperty('author')) { console.log(`нет свойства Автор`);
                } 
                else if(!key.hasOwnProperty('name')) { console.log(`нет свойства Имя`);
                } 
                else if(!key.hasOwnProperty('price')) { console.log(`нет свойства Цена`);
                } 
                else {
                    const li = document.createElement('li');
                    li.innerHTML = `<li>Автор: ${key.author}</li>
                                    <li>Название: ${key.name}</li>
                                    <li>Цена: ${key.price}</li>
                                    <br>`
                    ul.appendChild(li);
                }
        }
        catch (err) {
            console.log(err.message);
        }
    }
}

addNodes()


