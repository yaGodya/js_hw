function getRandomNumber() {
  return Math.floor(Math.random() * 130) + 1;
}
let randomNum = getRandomNumber();

const cardMain = document.querySelector(".main-section");

class Card {
  constructor(name, email, title, body) {
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
  }

  async getPostsAndUsers() {
    const usersResp = await fetch("https://ajax.test-danit.com/api/json/users");
    const users = await usersResp.json();

    const postsResp = await fetch("https://ajax.test-danit.com/api/json/posts");
    const posts = await postsResp.json();

    return posts.map((post) => ({
      ...post,
      user: users.find((user) => user.id === post.userId),
    }));
  }

  createCardElement(userPost) {
    const {
      title,
      body,
      user: { name, email },
    } = userPost;
    const cardPost = document.createElement("div");
    cardPost.classList.add("card");
    cardPost.innerHTML = `
      <div class="card-header">
        <div class="card-info">
          <p class="card-name">${name}</p>
          <a href="#" class="card-mail">${email}</a>
        </div>
        <div class="card-button-delete">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <div class="card-main">
        <div class="card-main-wrap">
          <h2 class="card-main-title">${title}</h2>
          <p class="card-main-text">${body}</p>
        </div>
      </div>
      <div class="card-footer">
        <div class="card-comments">
          <div class="card-comments-icon"></div>
          <div class="card-comments-text">${getRandomNumber()} тыс.</div>
        </div>
        <div class="card-retvit">
          <div class="card-retvit-icon"></div>
          <div class="card-retvit-text">${getRandomNumber()} тыс.</div>
        </div>
        <div class="card-likes">
          <div class="card-likes-icon"></div>
          <div class="card-likes-text">${getRandomNumber()} тыс.</div>
        </div>
        <div class="card-looking">
          <div class="card-looking-icon"></div>
          <div class="card-looking-text">${getRandomNumber()} млн</div>
        </div>
        <div class="card-share">
          <div class="card-share-icon">icon</div>
        </div>
      </div>`;
    return cardPost;
  }

  async createCards() {
    const userPosts = await this.getPostsAndUsers();
    const cardElements = userPosts.map(this.createCardElement);
    cardMain.append(...cardElements);
  }
}

const cards = new Card();
cards.createCards();
