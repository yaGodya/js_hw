const findBtn = document.querySelector(".findForIp ");
findBtn.addEventListener("click", function () {
    getIpAddress();
    findBtn.style.display = "none";
});

async function getIpAddress() {
    const ipResponce = await fetch("https://api.ipify.org/?format=json");
    const ipAdress = await ipResponce.json();
    const res = ipAdress.ip.toString();
    const youIp = await fetch(`http://ip-api.com/json/${res}`);
    const ip = await youIp.json();
    console.log(ip);

    const wrapper = document.querySelector(".wrapper");
    const findBlock = document.createElement("div");
    findBlock.classList.add("findBlock");
    findBlock.innerHTML = `
    <h2>Ваше местоположение</h2>
    <p>Страна:${ip.country}</p>
    <p>Город:${ip.city}</p>
    <p>Регион:${ip.region}</p>
    <p>Район:${ip.zip}</p>
    `;
    wrapper.appendChild(findBlock);
}
