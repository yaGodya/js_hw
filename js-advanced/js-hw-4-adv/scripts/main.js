
const ul = document.querySelector("ul")

function films() {
    return fetch("https://ajax.test-danit.com/api/swapi/films")
        .then(res => res.json())
        .then(list => {

            list.forEach(films => {
                let li = document.createElement("li")
                li.classList.add("film-list")
                ul.appendChild(li)
                let name = document.createElement("h3")
                name.classList.add("list-name")
                name.textContent = `Название: ${films.name}`
                li.appendChild(name)
                let episodeId = document.createElement("p")
                episodeId.textContent = `Номер эпизода: ${films.episodeId}`
                li.appendChild(episodeId)
                let openingCrawl = document.createElement("p")
                openingCrawl.textContent = `Краткое описание: ${films.openingCrawl}`
                li.appendChild(openingCrawl)
                films.characters.forEach((e) => {
                    fetch(e)
                        .then(res => res.json())
                        .then(data => {
                            let char = document.createElement("span")
                            char.textContent = `${data.name} , `
                            li.appendChild(char)
                        })
                })
                let underLine = document.createElement('br')
                li.appendChild(underLine)
            });
        })
}

films();
